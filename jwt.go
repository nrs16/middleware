package middleware

import (
	"errors"
	"fmt"
	"net/http"

	"log"

	"github.com/spf13/viper"

	"github.com/dgrijalva/jwt-go"
)

func GenerateJWT(claims jwt.MapClaims, secret string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign the token with a secret key to generate the final token string
	secretKey := []byte(secret) // Replace with your own secret key
	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ValidateJWT(r *http.Request) error {
	tokenString := r.Header.Get("Authorization")
	secret := viper.GetString("JWT_SECRET_KEY")

	s := []byte(secret)

	_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid_token")
		}
		return s, nil
	})

	if err != nil {
		return errors.New("invalid_token")

	}

	return nil
}

func ParseJWT(r *http.Request) (jwt.MapClaims, error) {

	tokenString := r.Header.Get("Authorization")
	secret := viper.GetString("JWT_SECRET_KEY")

	s := []byte(secret)

	jwtoken, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid_token")
		}
		return s, nil
	})

	if err != nil {
		return nil, errors.New("invalid_token")

	}

	if jwtoken.Valid {
		claims := jwtoken.Claims.(jwt.MapClaims)
		return claims, nil
	}

	return nil, errors.New("invalid_token")

}
func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		viper.AutomaticEnv()

		tokenString := r.Header.Get("Authorization")
		secret := viper.GetString("JWT_SECRET_KEY")
		log.Printf("secret: %v\n", secret)
		s := []byte(secret)

		_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				fmt.Println("token not signed correctly")
				ReplyErrorToClient(w, http.StatusInternalServerError, "Invalid Token", "invalid_token")
				return nil, nil
			}
			return s, nil
		})

		if err != nil {
			fmt.Printf("error parsing token: %+v\n", err.Error())
			ReplyErrorToClient(w, http.StatusInternalServerError, "Invalid Token", "invalid_token")
			return
		}
		next.ServeHTTP(w, r)
	})
}

package middleware

import (
	"github.com/dgrijalva/jwt-go"
)

type ErrorResponse struct {
	Message string `json:"message"`
	Code    string `json:"code"`
}

type EmailObject struct {
	Message    string   `json:"message"`
	Format     string   `json:"format"`
	Recipients []string `json:"recipients"`
	Subject    string   `json:"subject"`
}

type Configuration struct {
	General struct {
		Port       int  `toml:"Port"`
		PlugFields bool `toml:"PlugFields"`
	} `toml:"General"`
	Email struct {
		EmailAddress  string `toml:"email_address"`
		EmailPassword string `toml:"email_password_variable"`
		QueueName     string `toml:"queue_name"`
	} `toml:"Email"`
	RabbitMQ struct {
		Port     int    `toml:"Port"`
		Username string `toml:"Username"`
		Password string `toml:"Password"`
		IP       string `toml:"IP"`
	} `toml:"RabbitMQ"`
	Database struct {
		Port     int    `toml:"Port"`
		Username string `toml:"Username"`
		Password string `toml:"Password"`
		IP       string `toml:"IP"`
		Name     string `toml:"Name"`
	} `toml:"Database"`
	Logs struct {
		File bool   `toml:"File"`
		Path string `toml:"Path"`
	} `toml:"Logs"`
	SecondFactor struct {
		OTPLength int `toml:"OTPLength"`
	} `toml:"SecondFactor"`
}

type Logs struct {
	File bool   `toml:"File"`
	Path string `toml:"Path"`
}
type HTTPHeaders struct {
	UserAgent   string        `json:"User-Agent"`
	Accept      string        `json:"Accept"`
	ContentType string        `json:"Content-Type"`
	UUID        string        `json:"X-Correlation-ID"`
	Claims      jwt.MapClaims `json:"JWT-Claims"`
}

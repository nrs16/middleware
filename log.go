package middleware

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"reflect"

	"log/slog"
)

type Middleware func(http.Handler) http.Handler

func LoggerMiddleware(conf Logs) Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			log, err := Createlogger(conf)
			if err != nil {
				ReplyErrorToClient(w, http.StatusInternalServerError, err.Error(), "log_file_error")
				return
			}

			ctx := context.WithValue(r.Context(), "log", log)
			next.ServeHTTP(w, r.WithContext(ctx))

		})

	}
}

func Createlogger(conf Logs) (*slog.Logger, error) {
	var log *slog.Logger
	if conf.File {
		file, err := os.Create(conf.Path)
		if err != nil {
			return log, err
		}
		log = slog.New(slog.NewTextHandler(file, nil))
	} else {

		log = slog.New(slog.NewTextHandler(os.Stdout, nil))
	}

	return log, nil
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		headers := MapHeaders(r)
		log, ok := r.Context().Value("log").(*slog.Logger)
		if !ok {
			log = slog.New(slog.NewTextHandler(os.Stdout, nil))
		}

		log = log.With("uuid", headers.UUID)

		headerType := reflect.TypeOf(headers)

		for i := 0; i < headerType.NumField(); i++ {
			field := headerType.Field(i)
			log.Info(fmt.Sprintf("%s %s", field.Tag.Get("json"), reflect.ValueOf(headers).Field(i).Interface()))
		}
		next.ServeHTTP(w, r)

	})

}

func MapHeaders(r *http.Request) HTTPHeaders {
	var headers HTTPHeaders

	claims, _ := ParseJWT(r)
	headers.Claims = claims
	//ignoring because it will be caught in the previous function called in middlware

	headers.UserAgent = r.Header.Get("User-Agent")
	headers.Accept = r.Header.Get("Accept")
	headers.ContentType = r.Header.Get("Content-Type")
	headers.UUID = r.Header.Get("X-Correlation-ID")

	return headers

}

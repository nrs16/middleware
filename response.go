package middleware

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func ReplyErrorToClient(w http.ResponseWriter, statusCode int, message string, code string) {

	var response = ErrorResponse{Message: message, Code: code}
	data, err := json.Marshal(response)
	if err != nil {
		log.Printf("could not marshal data: %+v\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return

	}
	w.WriteHeader(statusCode)
	fmt.Fprint(w, string(data))
	return
}

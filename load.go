package middleware

import (
	"log"
	"os"

	toml "github.com/pelletier/go-toml"
)

func LoadConfig(path string) Configuration {
	var conf Configuration
	tomlFile, err := os.Open(path)
	if err != nil {
		log.Fatalf("error opening configuration file: %s\n", err.Error())
	}
	defer tomlFile.Close()

	if err := toml.NewDecoder(tomlFile).Decode(&conf); err != nil {
		log.Fatalf("error decoding conf file: %s\n", err.Error())
	}
	return conf
}
